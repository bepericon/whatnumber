import React from 'react';
import {StyleSheet, View, Button, Image} from 'react-native';
import Colors from '../constants/colors';

import Text from '../components/Text';
import MainButton from '../components/MainButton'

const GameOverScreen = (props) => {
  return (
    <View style={styles.screen}>
      <Text bold style={styles.title}>
        The game is over!
      </Text>
      <View style={styles.imageContainer}>
        <Image
          source={require('../assets/success.png')}
          //source={{uri: 'https://i1.wp.com/www.misfinanzasparainvertir.com/wp-content/uploads/2018/11/Tres-lecciones-de-negocios-que-sirven-para-subir-el-Monte-Everest.jpg?resize=696%2C466&ssl=1'}}
          // For web images ever is necesary set style: width and height.
          style={styles.image}
          resizeMode="cover"
        />
      </View>
      <View style={styles.resultContainer}>
        <Text style={styles.resultText}>
          Your phone needed
          <Text style={styles.highlight}> {props.roundsNumber} </Text>
          rounds to guess the number
          <Text style={styles.highlight}> {props.userNumber}</Text>
        </Text>
      </View>
      <MainButton onPress={props.onRestart}>NEW GAME</MainButton>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 150,
    borderWidth: 3,
    borderColor: 'black',
    overflow: 'hidden',
    marginVertical: 20,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  resultContainer:{
    marginHorizontal: 30,
    marginVertical: 10
  },
  resultText:{
    textAlign: 'center',
    fontSize: 20
  },
  highlight: {
    color: Colors.primary,
  },
});

export default GameOverScreen;
