import { getFontScale } from "react-native/Libraries/Utilities/PixelRatio";
export default {
    bold: 'OpenSans-Bold',
    regular: 'OpenSans-Regular',
};