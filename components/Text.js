import React from 'react';
import { Text as RNText, StyleSheet } from 'react-native';

import Fonts from '../constants/fonts';

const Text = props => {

    const { children, bold, style } = props;

    if(bold) return <RNText style={{...styles.bold, ...style}}>{children}</RNText>

    return <RNText style={{...styles.text,...style}}>{children}</RNText>
};

const styles = StyleSheet.create({
    text: {
        fontFamily: Fonts.regular 
    },
    bold: {
        fontFamily: Fonts.bold
    }
});

export default Text;